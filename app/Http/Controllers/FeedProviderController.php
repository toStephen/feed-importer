<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FeedProviderController extends Controller
{

  private $feedProviders;

  public function __construct()
  {
    // This is mocked data, should come out of DB normally
    $this->feedProviders = [
      'softwareadvice' => [
        'main_node' => 'products',
        'tags_node' => 'categories',
        'twitter_node' => 'twitter',
        'tittle_node' => 'title',
      ],
      'capterra' => [
        'tags_node' => 'tags',
        'twitter_node' => 'twitter',
        'tittle_node' => 'name',
      ]
    ];
  }

  /**
   * Returns feed providers with their contracted feed structure
   *
   * @return array
   */
  public function all()
  {
    return $this->feedProviders;
  }

  /**
   * Retrieves data related to the feed importer
   *
   * @return mixed
   */
  public function find($type)
  {
    $feedProviders = $this->feedProviders;
    if(isset($feedProviders[$type])) {
      return $feedProviders[$type];
    }
    abort(404, 'Feed provider not found');
  }
}
