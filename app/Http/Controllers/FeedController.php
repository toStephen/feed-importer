<?php

namespace App\Http\Controllers;
use App\Product;
use Symfony\Component\Yaml\Yaml;

class FeedController extends Controller
{
    /**
     * Import feed
     *
     * @return mixed
     */
    public function import($provider, $location)
    {
      $feedProvider = new FeedProviderController();
      $feedProvider = $feedProvider->find($provider);

      // Check if the file exists
      // TODO: now this only handles local files, in the future we need to check for URLs
      $path = public_path() . '/' . $location;
      $path_info = pathinfo($path);

      // Abort if the feed file is not located on the system
      (!is_file($path) ? abort(404, 'Feed file was not found in: ' . $path) : null);

      // Read file contents based on extension
      // TODO: add support for .csv
      switch ($path_info['extension']) {
        case 'json':
          $output = json_decode(file_get_contents($path), true); ;
          break;
        case 'yaml':
          $output = Yaml::parse(file_get_contents($path));
          break;
      }
      $result = $this->parseFeed($output, $feedProvider);
      return $result;
    }

  /***
   * Will parse the feed to a product object
   *
   * @param $output
   * @param $provider
   * @return string
   */
    private function parseFeed($output, $provider)
    {
      // Check if there is a product node available, if so, flatten it
      $output = (isset($provider['main_node']) ? $output[$provider['main_node']] : $output);
      $result = ''; // This will be the output for the terminal
      for ($i = 0; $i < count($output); $i++) {
        $cur = $output[$i];
        $product = new Product;
        $product->name = (isset($cur[$provider['tittle_node']]) ? $cur[$provider['tittle_node']] : null);
        $product->twitter = (isset($cur[$provider['twitter_node']]) ? $this->cleanTwitterHandle($cur[$provider['twitter_node']]) : null);
        $product->categories = (isset($cur[$provider['tags_node']]) ? $this->cleanTags($cur[$provider['tags_node']]) : null);
        $result .= $product->save() . PHP_EOL;
      }
      return $result;
    }

  /***
   * Will clean the tag values, if it receives a string, it will convert it to array so we clean the spacing
   *
   * @param $tags
   * @return string
   */
    private function cleanTags($tags){
      $tags = (is_string($tags) ?  explode(',',  $tags) : $tags);
      return join(", ", $tags);
    }

  /***
   * Will clean the twitter handle
   *
   * @param $handle
   * @return string
   */
    private function cleanTwitterHandle($handle){
      return (substr($handle, 0, 1) === '@' ? $handle : '@'.$handle);
    }
}
