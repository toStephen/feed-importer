<?php

namespace App\Console\Commands;

use App\Http\Controllers\FeedController;
use Illuminate\Console\Command;

/***
 *
 * Class that will handle console input to import a feed
 *
 * Class ImportCommand
 * @package App\Console\Commands
 */
class ImportCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @type string
     * @location string
     */
    protected $signature = 'feed:import {provider} {location}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Trigger a feed import';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      // Retrieve arguments
      $provider = $this->argument('provider');
      $location = $this->argument('location');

      $feedController = new FeedController();

      // Pass variables to FeedController
      $result = $feedController->import($provider, $location);
      $this->line($result);
    }
}
