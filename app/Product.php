<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
  public function save(array $options = [])
  {
    $atr = $this->getAttributes();
    return 'importing: Name: "' . $atr['name'] . '"; Categories: ' . $atr['categories'] . '; Twitter: ' . $atr['twitter'];
  }
}
