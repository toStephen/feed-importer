## Welcome to the feed importer!


###Instalation:
The set-up is quite straightforward:

1. Clone the project locally
2. Open up terminal and cd in to the folder
3. Run "composer install"
4. cp .env.example .env

### How to test the bash command:
1. cd in to the folder where the project lives
2. Run "php artisan feed:import softwareadvice feeds/softwareadvice.json"
3. Or run "php artisan feed:import capterra feeds/capterra.yaml"

### How to run tests:
1. cd in to the folder where the project lives
2. Run './vendor/bin/phpunit' in console

### Was it the first time I've worked with testing?
Yes, and no. I have some experience with testing in angular, but it's the first time I wrote a test in PHP. But I do not consider myself an expert on the area.