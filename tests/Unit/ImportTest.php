<?php

namespace Tests\Unit;

use App\Http\Controllers\FeedController;
use Tests\TestCase;

class ImportTest extends TestCase
{
  /**
   * Test if the folder where we store the feeds is readable
   *
   * @return void
   */
  public function testDirReadable()
  {
    $this->assertDirectoryIsReadable(public_path() . '/feeds');
  }

  /**
   * Test if the test feed import works and returns the exact amount of chars.
   *
   * @return void
   */
  public function testFeedImport(){
    $feed = '/feeds/testSA.json';
    $controller = new FeedController();
    $result = $controller->import('softwareadvice', $feed);
    $this->assertTrue(strlen($result) === 102);
  }
}
